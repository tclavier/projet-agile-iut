# Présentation générale

Description de la préparation à la semaine agile.
pour rappel, ceci concerne 3 modules de S4 
* Prog répartie (Y. Peter), 
* Prog web (T. Fritsch) 
* Prog mobile (J. Martinet)))

Sont concernés de plus loin:
* Prog web (P. Mathieu) en S3
* Prog système (M. Hauspie) : développement d'un serveur http en C !


Ce document est en trois parties:

1 Avancement pédagogique des 3 modules

2 Fonctionnalités prévues du squelette

3 Todo List
 
# Avancement pédagogique

## En prog répartie.

* Ce qui a été fourni au départ:
    * serveur rest + infrastructure Grizzly + Jersy + JPA
    
    * Avec tests sur les ressources et sur les accès BDD
    
    * fonctionnalités fournies: 
    
        * Ingrédients (resource + BDD)
    
        * Pizzas (BDD seulement)

* demandé:
    
    * ressources pizzas

    * spécification + BDD + ressource commandes


## En prog web / javascript (Thomas F peut compléter)

* Ce qui a été fourni:

    * Un jar comportant le serveur web + une base de donnée modifiées
    
    * un stock d'images (à installer sur le client)

  ** à compléter **

*  Demandé:
 
  ** à compléter **

* Correction fournie:
 
  ** à complèter **    

## En prog mobile (Android)

* Ce qui a été fourni:

    * Un jar comportant le serveur web + une base de donnée modifiées

    * Le source d'une appli qui interroge le serveur sous différents format
    
        * Génériques (Chaîne de chacractères, JSONArray, JSONObject)
        
        * Application (récupération d'un ingrédient en JSON)
        
* Demandé (mais pas toujours suivi):
    
    * Download la liste des ingrédients et chercher les pizzas qui ont un de ces ingrédients dans leur composition.         


# Proposition de squelette

* Un répertoire unique global

* un répertoire par module (java, javascript, androis)

* chacun des repertoires modules est structure selon les usages en cours dans le module mais

    * doit comporter les .gitignore pour ne suivre que les sources
    
    * comporte les description de projet/config (pom.xml, build.graddle, package.json, ...)
    
## assemblage

Le build graddle (android peut générer un apk qui serait importé dans le répertoire static du java/.../res/static)

Même chose pour le javascript qui recopie son index.html, le bundle javascript et les css dans le répertoire static

mvn package dans le projet java doit permettre de rassembler tous les morceaux ???

@Thomas: je débute en intégration continue, j'espère ne pas me faire constamment désintégré.. :)

    agilite/
    ├── android
    │   └── app
    │       ├── build.graddle (build android)
    │       ├── release
    │       │   └── xxxxx.apk (build apk)
    │       └── src
    │           └── main
    │               ├── java...
    │               └── res...
    ├── java
    │   ├── pom.xml (build maven pour java)
    │   └── src
    │       ├── main
    │       │   ├── java...
    │       │   └── res
    │       │       └── static (depot pour html static)
    │       └── test...
    └── javascript
        ├── build
        ├── css
        ├── images
        ├── js
        ├── package.json (build)
        ├── vscode
        └── webpack.config.js (build)


