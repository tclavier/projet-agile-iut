# Projet agile IUT de Lille

Dossier déplacé dans https://gitlab.com/univlille/startupweek/enseignants/secretariat

Le "projet agile" est le fruit d'une étroite collaboration entre la BGE et l'IUT de Lille. C'est un événement regroupant 2 promotions de l'IUT et des porteurs de projets suivi par la BGE.

# Objectif

Mettre les étudiants en situation d'entreprise innovante pour :
- les confronter à du code complexe au sens cynefin
- les confronter aux outils présent en entreprise (framework, langage, architecture logiciel, etc.)
- les confronter à l'auto-organisation, les décisions de groupe
- baisser leurs peurs d'entreprendre.

# Déroulement

- En janvier 
  - les groupes sont crés aléatoirement pour avoir une forme d'homogénéité de niveau.
  - les porteurs pitchs devant les étudiants
  - rencontre et echanges entre étudiants et porteurs
  - algo des mariages
- De janvier à fin Mars
  - Les gea travaillent 2h par semaine avec les porteurs
- Courant mars
  - 1 journée de formation pour les porteurs et 2h avec les étudiants : [description](journée-porteur.md)
- Fin mars, la semaine de projet
  - 5 jours de code avec des sprints de 2h
  - Un salon pour finir en beauté
  - une rétrospective de l'ensemble.

[planning des actions à mener](planning.md)


# Idées suite à la rétro 

- avoir 2 jours sans les GEA puis 3 jours avec les GEA à temps plein.
- impliquer les GEA durant la journée de formation PO : légo4scrum puis backlog 
- impliquer les étudiants d'info pour le backlog en fin de journée PO


