# Objectif de ce document

Donner à un encadrant les moyens d'être autonome en considérant qu'il part avec un bagage minimum décrit ci dessous.

## Prérequis

On suppose que les encadrants ont quelques connaissances en agilité, qu'ils savent annimer une rétrospective et qu'ils maitrise au moins une des piles technique (Javascript, Java JAX-RS, Androïd)
Les encadrants doivent être capable de dire aux étudiants : Je ne sais pas et de les aider à trouver une solution, c'est alors la démarche de recherche que l'on transmetra aux étudiants.

Sans être une obligation, il est préférable de maîtriser l'une ou l'autre de ces techniques :

- découpage de backlog par la valeur (slicing) 
- TDD
- BDD
- Annimation de groupe

## Bibliographie

Une petite liste de livres de base sur le sujet : 

* Agile retrospectives
* Extreme Programming Explained
* Kanban pour l'IT de Laurent Morisseau
* La pratique du lean management dans l’IT 
* Running Lean 
* Scrum de claude aubry
* Scrum from the trench
* Solution Focus de Géry Derbier
* Start with Why de Sinon Sinek
* The Lean Startup
* Turn the Ship Around! de David Marquet

# Vocabulaire 

## User Story
Histoire utilisateur en français, une User Story (US) est un postit décrivant un petit morceau de travail à faire en moins d'1h30. Une US doit faire apparaitre les éléments suivants : 
- qui 
- quoi
- pour quoi

Une trame courrement utilisé est : En tant que **Qui** Je souhaite **Quoi** afin de **Pour quoi**

Toutes les histoires utilisateurs se doivent d'être les plus petites possible et indépendante les unes des autres.

# Les contraintes techniques

- Un serveur REST en java pour gérer toute la logique métier (JAX-RS).
- Un client Javascript pour un affichage au moins PC
- Un client Androïd

Tout le code doit être dans git.

# Le rôle de l'encadrant sur le projet est multiple.

- Apporter un support technique aux étudiants qui le demande que ce soit sur l'archie, Androïd, Java côté serveur, Javascript ou git.
- Apporter un œil critique sur leur code (genre s'ils sont obligés d'écrire des commentaires pour expliquer leur code ou si c'est plein de copier coller)
- Animer les démos et les rétrospectives
- Ouvrir et fermer les salles :-) 
- Remplir le document de suivi, je vous le donnerais plus tard dans le slack

# Demis journée type

- 8h00 ouverture de la salle, début du premier sprint de la demis journée
- 9h30 -> 10h00 démo puis rétro dans chaque équipe

Il est indispensable que l'équipe ne parte pas en pause avant le passage du prof.

- 10:00 début du sprint suivant
- 11:30 : démo équipe par équipe avec toute la salle
- 11:45 : rétro de toute la salle.

Tout le monde peut partir manger.

## Démo

Une démo c'est l'équipe qui présente son travail depuis la dernière démo du même type.

## Rétro

L'objectif c'est de voir ce qui fonctionne bien et ce qu'il faut améliorer dans leur façon de travailler. À la fin de la rétro, l'équipe doit avoir 1 truc à tester durant l'itération suivante et avoir un moyen de mesurer si le test en question était utile ou pas pour prendre une décision 2h plus tard.

Sur le radiateur d'information, on doit voir le point d'amélioration sélectionné à la dernière rétro, comment il sera mesuré et ce que l'on en attend.

### En petit groupe (9h30 et 15h00)

Une façon de faire en petit groupe c'est de demander au groupe sans bâton de parole :
- Qu'avez-vous tester durant ce sprint ? (Normalement c'est visible sur leur radiateur d'informations)
- Quelle décision prenez-vous ? 

Puis de faire passer un bâton de parole pour répondre question par question :

- D'après toi, qu'est-ce qui a bien fonctionné durant ce sprint ?
- D'après toi, que faudrait-il améliorer ?

Résumer les éléments à améliorer puis demander sans le bâton de parole :

- D'après vous, quel est le principal éléments que vous voulez améliorer au prochain sprint ? 
- Donnez moi 3 options, 3 expériences que vous pourriez tester durant le prochain sprint pour améliorer ce point ?
- À quoi verrez-vous que c'était utile ?
- Quel option voulez-vous tester durant le prochain sprint ?

### En grand groupe (11h30 et 17h00)

Une façon de faire c'est de leur demander équipe par équipe devant tout le monde :

- Qu'avez-vous tester durant ce sprint ?
- Quelle décision prenez-vous ?

Puis, leur demander d'écrire chacun sur un postit 1 truc à garder et sur un autre postit 1 truc à améliorer. Les faire coller au tableau avant de regrouper les postit.

Puis par équipe :

- D'après vous, quel est le principal éléments que vous voulez améliorer au prochain sprint ?

Puis à toutes les équipes : 

- Donnez 3 options, 3 expériences que vous pourriez tester durant le prochain sprint pour améliorer ce point ?
- À quoi verrez-vous que c'était utile ?

Enfin par équipe :

- Quel option voulez-vous tester durant le prochain sprint ?

## Cas particulier du premier jour

De 8h00 à 9h30 le but c'est :
- de partager le baklog dans l'équipe, que chacun soit capable de parler du projet et d'expliquer quel sont les trucs qui vont sortir et dans quel ordre.
- d'estimer les Users Stories en points d'efforts
- de découper les US trop grosse (pas faisable en moins de 2h) en US plus petite

à la démo de 9h30, je m'attends à entendre une autre personne que le porteur expliquer le projet et présenter le burndownchart à peine initialisé.

De 10h00 à 11h30, le but c'est :

- d'initialiser la pile technique et de produire un "Hello World!" qui utilise toutes les briques technique : BDD, Java REST, Android, Javascript. 

À la démo de 11h30 je m'attends à avoir un membre de l'équipe qui présente le projet, le radiateur d'information et une démo du "Hello World!". Si en plus il y a d'autres US de livré, c'est la classe !

# Exemple expliqués 

## Rétro

Le but de la rétro c'est que l'équipe trouve ensemble des solutions pour améliorer leur façon de travailler ensemble.
Et il faut l'animer
Par exemple :
- *Moi*, que voulez les vous améliorer au prochain sprint ? 
- l'équipe, "rien, tout marche nickel". 
- Donc vous avez livré tout ce que vous avez promis au porteur, vous avez eu l'impression de ne pas perdre une minute ? vous avez tout compris du premier coup ?

En gros je me le doigt sur un ou des trucs qui n'ont pas fonctionné parfaitement

- l'équipe, effectivement il y a un truc qui n'a pas bien fonctionné, c'est git
- et que pouvez vous faire pour améliorer ça ?
- rien, le prof était nul
- moi, Vous ne pouvez vraiment rien faire ?
- équipe après de nombreuses questions de ma part : si, on pourrait lire de la doc
- moi, quoi d'autre ?
- équipe, on pourrait vous demander un cours de rattrapage
etc.

à la fin on a un truc à améliorer : ils n'arrivent pas à faire du git correctement.
et 3 options pour résoudre le problème : demander un cours, lire de la doc et une autre option.

Exemples de trucs que l'équipe pourrait avoir envie d'améliorer :

- on a du mal à se mettre d'accord,
- on a fait des promesses trop grosses
- machin fait chier
- on a commencé 7 taches en même temps et on en a fini aucune
- etc.

et dans les options on aurait put avoir :
- on va tester le pairring
- on va faire un schéma d'archi pour se mettre d'accord
- etc.


Attention à l'intégration total de la classe ! Peut-être leur demander si tout le monde à bien participé, atention vous êtes une équipe.

