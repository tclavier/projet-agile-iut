La rétrospective d'activité se fait avec tout le monde, en world café.

## En salle de TD : 

On met les étudiants par table de 4 ou 5, ils définissent un rapporteur de table.

Expliquer l'objectif : faire le bilan de l'activité, comprendre ce qui a bien fonctionné et ce que l'on devrait améliorer. En tant d'animateur de la rétrospective votre rôle n'est pas de répondre aux questions sur l'activité mais de récolter leur feedback. En cas de question, il est important de les noter, nous prendrons le temps d'y répondre en amphi.

Première question : 
- racontez entre vous (par table) des histoires, des anecdotes qui se sont déroulés durant cette semaine agile qui évoquent chez vous de bons souvenirs. Essayer de trouver une histoire par jour.

> Pendant les échanges, passer de table en table et vérifier que ce sont bien des histoires qui sont échangés, dans le cas contraire, demander des anecdotes qui illustre leurs propos.

10 min plus tard, on stop les échanges. Et on demande à chaque table s'ils veulent partager une histoire avec toutes les tables.

On mélange les tables, seul le rapporteur ne bouge pas. 

Chaque table va alors avoir 10 minutes pour raconter ce qui c'est passé sur leur table et répondre à la seconde question : 
- racontez entre vous des histoires qui se sont déroulés durant cette semaine et qui évoquent chez vous de la colère, de la frustration ou de la déception, en résumé un sentiment négatif. Essayer de trouver une histoire par jour. Il est important de raconter des histoires pour avoir des éléments observable, des illustrations concrètes des problèmes rencontrés, par exemple à la place de "Il faut que je grandisse" un truc du genre "J'ai calculé mon IMC et il n'est pas bon du tout" permetra de trouver ensemble plusieurs options pour résoudre le prblème, par exemple maigrir.

entre 10 et 15 min plus tard, on stop les échanges. 
On demande à chaque table s'ils veulent partager une histoire avec toutes les tables s'il reste plus de 30 minutes.
On mélange les tables, seul le rapporteur ne bouge pas. 

Troisième question : 
- En vous inspirant des histoires que vous avez raconté, construire la liste des choses à garder et la liste des choses à améliorer. Pour nous les profs.

entre 10 et 15 min plus tard, on stop les échanges. 

Trouver un rapporteur de salle pour partager en amphi.

On récolte les idées de toutes les tables en les écrivant sur le tableau pour les présenter en amphi après. 
Une façon de faire c'est de découper le tableau en 2 : à améliroer, à garder. Puis de demander à chaque table quel est l'élément le plus important pour eux dans l'une ou l'autre des catégories. 
Puis faire le tour de toutes les tables jusqu'à épuisement des idées. L'aventage de cette technique, c'est que les points les plus importants sont forcément en haut.
Le rapporteur de salle peu alors prendre une photo.

On mélange les tables, seul le rapporteur ne bouge pas. 

Dernière question : 
- Imaginez, c'est le début de votre stage, probablement dans une semaine, que voyez-vous de changé par rapport à avant le début du projet agile ? Qu'allez vous faire de différent ? 

entre 10 et 15 min plus tard, on stop les échanges. 
On récolte les idées de toutes les tables pour les présenter en amphi après.


## En amphi

Les rapporteurs de salle passent les un après les autres pour regrouper les choses à garder et les choses à améliorer.
