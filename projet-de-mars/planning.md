# Septembre ou octobre

- Monter le slack avec tous les étudiants infos
- Planifier en décembre la demis journée à la BGE de présentation des projets
- Planifier en décembre la demis journée chez 1kubator de présentation des projets

# Novembre

- Planifier en janvier la demis journée de pitch devant les étudiants d'info et GEA
- Planifier en janvier un atelier GEA / porteurs : 0.5j
- planifier la semaine agile dans l'agenda des S4
- réserver les salles pour la semaine
  * amphi 30 min pour l'ouverture
  * salle de DS pour la rétro
  * salles de TP avec android
- libérer les S2 pour le salon
- libérer les S4 GEA la demis journée de formation PO

# Décembre

- Demis journée de présentation des projets 
- Jury de sélection des projets
- Annonce du planning aux porteurs

# Janvier

- Constituer les équipes
- demis journée de pitch devant les étudiants
- répartition porteurs / équipes
- atelier GEA porteurs
- Initiation au vocabulaire agile pour les porteurs et GEA. 1 jour pour les porteurs, 0.5j pour les GEA.
- laisser les porteurs travailler le backlog pour le retravailler en mars juste avec la semaine
- réserver le hall ou le centre de resources pour le salon
- prévenir banquiers et anciens porteurs pour le salon
- prévenir la presse pour mars
- réserver la classe mobile pour le jour du salon

# Mars

- rappel de la presse
- préparer le matériel : 
  * magic chart
  * feutres
  * post-it
  * support pour les affiches
- Répartition des équipes dans les salles de TP
- Préparer des affiches avec les nom des équipes à mettre sur les portes de salle de TP
- Préparer les slides d'intro en amphi

Resultat, un screencast des applications.

