à déplacer sur le site ... peut-être en pdf

L’IUT A Informatique et GEA de Lille est heureux de vous convier au salon de clôture de la "startup week" 2020 

Rendez-vous
Le 25 mars 2020 à 16h30
À l’IUT A de Lille, Bd Paul Langevin
Cité Scientifique à Villeneuve d’Ascq

L’IUT A Informatique et GEA de Lille s'associe avec le réseau d’appui aux entrepreneurs BGE Hauts de France et à l'incubateur 1kubator pour faire vivre aux étudiants et à des chefs d’entreprises une semaine en mode start-up. Organisés en équipe, étudiants et porteurs de projets travailleront sur le développement d’une application ou d’un logiciel, permettant aux entrepreneurs de trouver une solution numérique en phase avec leur modèle économique. Le principe de travail est celui du mode agile* (ou « Lean Start up »), rythmé par des itérations de deux heures. L’objectif est de créer un maximum de valeur en un minimum de temps. Cette année, ce sont pas moins de 11 projets qui seront travaillés. Une formation à l'agilité est dispensée en amont par les enseignants de l’IUT aux chefs d’entreprise, afin de mettre en pratique ces nouvelles compétences et de jouer au mieux leur rôle de 'product owner', lors de cette startup week.

Le salon est une présentation des résultats de la semaine, tant sur le volet pédagogique qu’opérationnel en présence des étudiants et des chefs d’entreprise mercredi 25 mars de 16h30 à 18h00

* La startup week est réalisé dans le cadre du projet de fin de formation du DUT Informatique et GEA de l'IUT A. Durant 5 jours, les étudiants travaillent sur un projet à plein temps (8h/jour) en utilisant une approche agile basée sur des itérations (très) courtes de 2h. Cette méthode permet d'avoir un prototype fonctionnel dès les premières itérations, ce qui facilite les retours du client. Cette activité permet à la fois aux étudiants de découvrir le travail en équipe de 6-8 étudiants – et donc les problématiques d'auto-organisation - et en parallèle aux créateurs d’entreprise d’être formés à la gestion de projet selon Démarche Agile selon un mode d’apprentissage « Learning by Doing ».

Plus d'information sur : http://startupweek.fr/

Contacts :
* Thomas CLAVIER <thomas.clavier@univ-lille.fr> (PAST à l'IUT "A" de l'Université de Lille - Sciences & Technologies et fondateur d'AZAÉ) // 06 20 81 81 30
* Arnaud SIBILLE <a.sibille@bge-hautsdefrance.fr>, Responsable territorial BGE Hauts de France // 06 73 42 03 03 
* Yann SECQ <yann.secq@univ-lille.fr> (E-C en Informatique à l'IUT "A" de l'Université de Lille - Sciences & Technologies)
* Valerie FRANCOIS <valerie.francois@univ-lille.fr> (Maître de Conférences - HDR à l'IUT "A" de l'Université de Lille - Sciences & Technologies)
