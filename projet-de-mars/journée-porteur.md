# Invitation

Pour les plus impatients ... quelques pointeurs
* https://www.youtube.com/watch?v=502ILHjX9EE

# Journée porteurs PO

## Objectif

- Partager du vocabulaire
- Vous donner les billes pour préparer un bon backlog
- Vous préparer à votre nouveau rôle de PO
- Embarquer les GEA

## Matinée avec les GEA

- Pourquoi l'agilité : Cynefin + slides thales
- Rôle du PO => vidéo de kniberg
    - refaire le schéma et demander qui sont les partis prenantes
- Artistes et spécifieurs
- Atelier lean-startup
    - drono-poste
    - de quel façon on va pouvoir tester tel hypothèse ?

## Après midi

- Atelier story mapping
    - exo en groupe de 4
    - exo sur leur projet en binôme








