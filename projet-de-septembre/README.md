# Mini projet agile

Documents déplacés dans https://gitlab.com/tclavier/projet-agile-de-septembre-iut-de-lille

## Objectifs

* Remettre les étudiants dans le bain de la programmation. 2 mois dans code c'est long.
* Remettre les étudiants dans le bain de la gestion de code source en équipe.
* Créer de la cohésion dans la promo.
* Faire une très courte découverte de l'agilité.

## Déroulé

L'activité se déroulant sur 3j (2,5j en 2019)

* un amphi d'introduction d'une heure environ : [les supports](https://gitlab.com/tclavier/intro-projet-agile-iut/blob/master/n3p1fi.pdf)
* une session de découverte (2h) de l'agilité via un jeu sérieux Lego4Scrum [les notes pour les animateurs](https://gitlab.com/tclavier/lego4scrum/builds/artifacts/master/file/notes_pour_le_facilitateur.pdf?job=build_pdf)
* 4 demi-journées structurées en sprint de 2h
  * 1h30 de développement
  * 10mn de démo
  * 10mn de rétrospective
* une demi-journée de conclusion
  * 2h de présentation aux premières années (10 min par salles)
  * 2h de rétrospective sur l'activité (salle de DS)

* Présentation générale des expérimentations menées autour de l'initiation à la démarche agile au département
  * https://github.com/tclavier/rex-iut-agile-laval
  * https://github.com/tclavier/rex-iut-agile-laval/blob/master/CLAVIER_SECQ_AgileLaval16.pdf
* Support d'introduction de l'amphi
  * https://gitlab.com/tclavier/intro-projet-agile-iut
* Synopsis Lego4Scrum
  * https://gitlab.com/tclavier/lego4scrum

## Notes pour les intervenants

Nous avons décidé collectivement les années précédentes de faire l'appel, le documents google partagé permet de le faire facilement.

[Support de mars pour les intervenats](projet-de-mars/support-intervenants.md) on y trouve entre autre quelques pistes pour animer les rétrospectives.
