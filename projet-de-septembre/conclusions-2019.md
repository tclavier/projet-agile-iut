# À garder
- les groupes imposés
- choix libre du sujet
- retrospectives
- 2 jours et demi de projet
- nombre d'equipiers
- garder le mode texte
- un prof pour les démos
- démos
- les post-it pour les backlog
- 2 jours non stop
- juste à la rentrée

# À changer
- sprint 0 trop court, sprint 1 utilisé pour la mise en place de git etc
- faire le lego4scrum en codant
- aller voir les projets des autres salles
- cours de GIT avant le projet
- mêmes groupes pdt les lego et les projets
- mieux organiser la rencontre avec les S1
- ne pas obliger le mode texte
- éviter la contradiction entre les professeurs
- retros trop longues
- rétro globale trop longue
- plusieurs couleurs de post-it
- temps de préparation du sprint

# Le test de l'année

- Faire un travail sur le manifest agile en intro de rétrospective.

# Idées de changements à tester

- Organiser une demis journée avec l'équipe avant le projet pour faire un légo et s'aligner, se remettre dans le bain.
- faire 4h de sprint 0 et 0' avec dedans :
  * Trouver une idée
  * Découper le backlog
  * Initialiser le dépos
  * faire l'archi du projet
- Proposer au BDE de faire des sandwitchs temps que le kiosk et fermé
