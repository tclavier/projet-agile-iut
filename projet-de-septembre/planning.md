# Juin

- Valider l'équipe pédagogique avant les vacances
- libérer les S1 pour la démo finale
- réserver les salles pour les 3 jours
  * amphi 1h
  * nombre de profs * TD 2h pour le légo
  * salles de TP
  * salle de DS pour la rétro
- Planifier la demis journée alignement.

# Fin aout

- faire une demis journée d'alignement avec l'équipe

# Septembre

- Faire les groupes avec le classement de S2 (FI + FC)
- Faire le groupe gitlab avec les projets
- Répartition des équipes dans les salles de TD et TP
- préparer le document d'appel, par groupe et par salle.
- Préparer les slides d'intro en amphi
- préparer le matériel :·
  * magic chart
  * feutres
  * post-it
- préparer le slack / mattermost
