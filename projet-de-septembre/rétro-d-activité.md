La rétrospective d'activité se fait avec tout le monde, en world café.

## En salle de DS

On met les étudiants par table de 4 ou 5, ils définissent un rapporteur de table.


Expliquer l'objectif : faire le bilan de l'activité, comprendre ce qui a bien fonctionné et ce que l'on devrait améliorer, identifier nos apprentissages et comprendre comment les améliorer.

Distribuer à chaque table le manifest agile.

Questions :
- Lire le manifest agile et racontez des histoires, des anecdotes qui montrent que vous avez été agile. Chaque histoire va venir illustrer l'un des principes. Au moins 2 ou 3 histoires par personne, pour chaque histoire, identifiez le principe illustré.
- racontez entre vous (par table) des histoires, des anecdotes qui se sont déroulés durant ces 2 jours et demis, pour chaque histoire, identifiez si c'était positif ou négatif.
- En vous inspirant des histoires que vous avez raconté, construire la liste des choses à garder et la liste des choses à améliorer. Pour nous les profs.
- Raconter entre vous des histoires de ce que vous ferez dès demain pour être agile. identifiez le principe illustré par chaque histoire.

